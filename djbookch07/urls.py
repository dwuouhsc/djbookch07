from django.conf.urls import *
#from django.conf.urls import patterns, include, url, views
from books.views import search, contact

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^djbookch07/', include('djbookch07.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^$', include(djbookch07.views)),
    url(r'^search/', search),
    url(r'^contact/', contact),
     
)
