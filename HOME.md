# CHapter 07 Form Processing

1) cd /Users/DeeWu/src/Django_Attempt01/my_blog_env
2) bin/django-admin.py startproject djbookch07 
3) activate the sqlite3 and generate location of DB  by modifying settings.py
   (vi djbookch07/settings.py)
4) change djbookch07/url.py adding:
      url(r'^search/', include(djbookch07.books.views)),
5) create book app
../bin/python manage.py startapp books

remember to update the INSTALLED APPS in setting.py

6) fix: /Users/DeeWu/src/Django_Attempt01/my_blog_env/djbookch07/books/models.py
(see the example from djbooksch05 example)a
7) create djbookch07p97.html inside of /Users/DeeWu/src/Django_Attempt01/my_blog_env/templates, remember to include that directory into settings.py TEMPLATE_DIRS
8) create forms.py inside of app (see snippet 5) 

9) ../bin/python manage.py sqlall books
BEGIN;
CREATE TABLE "books_publisher" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(30) NOT NULL,
    "address" varchar(50) NOT NULL,
    "city" varchar(60) NOT NULL,
    "state_province" varchar(30) NOT NULL,
    "country" varchar(50) NOT NULL,
    "website" varchar(200) NOT NULL
)
;
CREATE TABLE "books_author" (
    "id" integer NOT NULL PRIMARY KEY,
    "salutation" varchar(10) NOT NULL,
    "first_name" varchar(30) NOT NULL,
    "last_name" varchar(40) NOT NULL,
    "email" varchar(75) NOT NULL,
    "headshot" varchar(100) NOT NULL
)
;
CREATE TABLE "books_book_authors" (
    "id" integer NOT NULL PRIMARY KEY,
    "book_id" integer NOT NULL,
    "author_id" integer NOT NULL REFERENCES "books_author" ("id"),
    UNIQUE ("book_id", "author_id")
)
;
CREATE TABLE "books_book" (
    "id" integer NOT NULL PRIMARY KEY,
    "title" varchar(100) NOT NULL,
    "publisher_id" integer NOT NULL REFERENCES "books_publisher" ("id"),
    "publication_date" date NOT NULL
)
;
CREATE INDEX "books_book_22dd9c39" ON "books_book" ("publisher_id");
COMMIT; 

10) ../bin/python manage.py syncdb
Creating tables ...
Installing custom SQL ...
Installing indexes ...
Installed 0 object(s) from 0 fixture(s) 


11) 



